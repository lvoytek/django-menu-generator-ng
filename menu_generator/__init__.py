__author__ = 'Milton Lenis'
__description__ = 'A straightforward menu generator for Django'
__version__ = '1.2.3'

default_app_config = 'menu_generator.apps.MenuAppConfig'
